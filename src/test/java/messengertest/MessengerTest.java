package messengertest;

import com.fasterxml.jackson.databind.ObjectMapper;
import messenger.json.Conversation;
import messenger.ConversationLoader;
import nametag.StringReplacer;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MessengerTest {
    private String jsonMessage = "{\n" +
            "  \"participants\": [\n" +
            "    {\n" +
            "      \"name\": \"Doris Zelmanova\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"name\": \"Ondrej Sotolar\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"messages\": [\n" +
            "    {\n" +
            "      \"sender_name\": \"Doris Zelmanova\",\n" +
            "      \"timestamp_ms\": 1552248882738,\n" +
            "      \"content\": \"Text 1\",\n" +
            "      \"type\": \"Generic\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sender_name\": \"Doris Zelmanova\",\n" +
            "      \"timestamp_ms\": 1552248695498,\n" +
            "      \"content\": \"Text 2\",\n" +
            "      \"type\": \"Generic\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"title\": \"Doris Zelmanova\",\n" +
            "  \"is_still_participant\": true,\n" +
            "  \"thread_type\": \"Regular\",\n" +
            "  \"thread_path\": \"inbox/DorisZelmanova_NZGcHU1EmA\"\n" +
            "}";

    @Test
    public void parseMessageString() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Conversation actual = objectMapper.readValue(jsonMessage, Conversation.class);

        Assert.assertEquals(actual.getTitle(), "Doris Zelmanova");
        Assert.assertEquals(actual.getThread_path(), "inbox/DorisZelmanova_NZGcHU1EmA");
        Assert.assertEquals(actual.getMessages().get(0).getContent(), "Text 1");
        Assert.assertEquals(actual.getMessages().get(0).getSender_name(), "Doris Zelmanova");
        Assert.assertEquals(actual.getMessages().get(1).getContent(), "Text 2");
        Assert.assertEquals(actual.getMessages().get(1).getSender_name(), "Doris Zelmanova");
    }

    @Test
    public void loadConversationFromFile() throws IOException {
        ConversationLoader fl = new ConversationLoader();
        Conversation conversation = fl.getConversationFromFile(Paths.get("src","test","resources"), true);

        Assert.assertEquals(conversation.getMessages().get(0).getSender_name(), "Ondřej Sotolář");
        Assert.assertEquals(conversation.getMessages().get(1).getSender_name(), "Martin Malaník");
    }

    @Test
    public void savePlainTextFile() throws IOException {
        ConversationLoader fl = new ConversationLoader();
        Conversation conversation = fl.getConversationFromFile(Paths.get("src","test","resources"), true);

        Path testPath = Paths.get("src","test","resources", "testfile.txt");
        fl.saveToFile(testPath, fl.extractConversationContent(conversation));
    }

    @Test
    public void savePlainTextFile2() throws IOException {
        ConversationLoader fl = new ConversationLoader();
        Conversation conversation = fl.getConversationFromFile(
                Paths.get("src","test","resources", "messenger-conversations", "mayhameggs.json"), false);

        Path testPath = Paths.get("src","test","resources","messenger-conversations", "mayhameggs_plaintext.txt");
        String conversationString = fl.extractConversationContent(conversation);
        StringReplacer stringReplacer = new StringReplacer();
        String emojiless = stringReplacer.cleanStringOfEmojis(conversationString);
        fl.saveToFile(testPath, emojiless);
    }

    @Test
    public void savePlainTextFile_vojtechbrezik() throws IOException {
        ConversationLoader fl = new ConversationLoader();
        Conversation conversation = fl.getConversationFromFile(
                Paths.get("src","test","resources", "messenger-conversations", "vojtechbrezik"), true);

        Path testPath = Paths.get("src","test","resources","messenger-conversations", "vojtechbrezik", "vojtechbrezik_plaintext.txt");
        Path annPath = Paths.get("src","test","resources","messenger-conversations", "vojtechbrezik", "vojtechbrezik_plaintext.ann");
        String conversationString = fl.extractConversationContent(conversation);
        StringReplacer stringReplacer = new StringReplacer();
        String emojiless = stringReplacer.cleanStringOfEmojis(conversationString);
        fl.saveToFile(testPath, emojiless);
        fl.saveToFile(annPath, "");
    }

    @Test
    public void savePlainTextFile_lenkavavrusova() throws IOException {
        ConversationLoader fl = new ConversationLoader();
        Conversation conversation = fl.getConversationFromFile(
                Paths.get("src","test","resources", "messenger-conversations", "lenkavavrusova"), true);

        Path testPath = Paths.get("src","test","resources","messenger-conversations", "lenkavavrusova", "lenkavavrusova_plaintext.txt");
        Path annPath = Paths.get("src","test","resources","messenger-conversations", "lenkavavrusova", "lenkavavrusova_plaintext.ann");
        String conversationString = fl.extractConversationContent(conversation);
        StringReplacer stringReplacer = new StringReplacer();
        String emojiless = stringReplacer.cleanStringOfEmojis(conversationString);
        fl.saveToFile(testPath, emojiless);
        fl.saveToFile(annPath, "");
    }

    @Test
    public void savePlainTextFile_karelvasak() throws IOException {
        ConversationLoader fl = new ConversationLoader();
        Conversation conversation = fl.getConversationFromFile(
                Paths.get("src","test","resources", "messenger-conversations", "karelvasak"), true);

        Path testPath = Paths.get("src","test","resources","messenger-conversations", "karelvasak", "karelvasak_plaintext.txt");
        Path annPath = Paths.get("src","test","resources","messenger-conversations", "karelvasak", "karelvasak_plaintext.ann");
        String conversationString = fl.extractConversationContent(conversation);
        StringReplacer stringReplacer = new StringReplacer();
        String emojiless = stringReplacer.cleanStringOfEmojis(conversationString);
        fl.saveToFile(testPath, emojiless);
        fl.saveToFile(annPath, "");
    }

    @Test
    public void savePlainTextFile_doriszelmanova() throws IOException {
        ConversationLoader fl = new ConversationLoader();
        Conversation conversation = fl.getConversationFromFile(
                Paths.get("src","test","resources", "messenger-conversations", "doriszelmanova"), true);

        Path testPath = Paths.get("src","test","resources","messenger-conversations", "doriszelmanova", "doriszelmanova_plaintext.txt");
        Path annPath = Paths.get("src","test","resources","messenger-conversations", "doriszelmanova", "doriszelmanova_plaintext.ann");
        String conversationString = fl.extractConversationContent(conversation);
        StringReplacer stringReplacer = new StringReplacer();
        String emojiless = stringReplacer.cleanStringOfEmojis(conversationString);
        fl.saveToFile(testPath, emojiless);
        fl.saveToFile(annPath, "");
    }

    @Test
    public void anotherDecode() {
        String text = "Ond\u00c5\u0099ej Sotol\u00c3\u00a1\u00c5\u0099";
        String correctText = "Ondřej Sotolář";
        System.out.println(correctText);
        final Charset fromCharset = Charset.forName("windows-1252");
        final Charset toCharset = Charset.forName("UTF-8");
        String fixed = new String(text.getBytes(fromCharset), toCharset);

        Assert.assertEquals(correctText, fixed);
    }

    @Test
    public void decodeBetter() {
        String text = "Ond\u00c5\u0099ej Sotol\u00c3\u00a1\u00c5\u0099";
        String correctText = "Ondřej Sotolář";

        ConversationLoader conversationLoader = new ConversationLoader();
        String unescaped = conversationLoader.unescapeMessenger(text);
        byte[] bytes = unescaped.getBytes(StandardCharsets.ISO_8859_1);
        String fixed = new String(bytes, StandardCharsets.UTF_8);

        Assert.assertEquals(correctText, fixed);
    }

    @Test
    public void testNewline() {
        String n = "\n";
        Assert.assertEquals(1, n.length());
        Assert.assertEquals("\n", n.substring(0, 1));
        Assert.assertEquals('\n', n.charAt(0));
    }
}
