package anonymizationtest;

import nametag.NerEntity;
import org.junit.Assert;
import org.junit.Test;
import rulematch.RulesMatcher;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

public class RulesMatcherTest {
    @Test
    public void removeOverlap() {
        RulesMatcher rulesMatcher = new RulesMatcher();

        NerEntity n1 = new NerEntity(0,5);
        NerEntity n2 = new NerEntity(0,5);
        NerEntity n3 = new NerEntity(0,3);
        NerEntity n4 = new NerEntity(1,2);
        NerEntity n5 = new NerEntity(4,10);
        NerEntity n6 = new NerEntity(10,12);
        NerEntity n7 = new NerEntity(11,12);

        List<NerEntity> list1 = Arrays.asList(n1, n2, n3, n4, n5, n6, n7);
        TreeSet<NerEntity> result1 = rulesMatcher.removeOverlaps(list1);
        Assert.assertEquals(n1, result1.first());
        Assert.assertEquals(n6, result1.last());
        Assert.assertEquals(2, result1.size());

        List<NerEntity> list2 = Arrays.asList(n4, n5);
        TreeSet<NerEntity> result2 = rulesMatcher.removeOverlaps(list2);
        Assert.assertEquals(n4, result2.first());
        Assert.assertEquals(2, result2.size());

        List<NerEntity> list3 = Arrays.asList(n6, n5);
        TreeSet<NerEntity> result3 = rulesMatcher.removeOverlaps(list3);
        Assert.assertEquals(n5, result3.first());
        Assert.assertEquals(2, result3.size());


    }

    @Test
    public void testData() {
        String email = "Jinak sítě na oknech máme, balkon je také zasíťovaný, protože přítel měl dříve králíka, který měl volný pohyb po celém bytě, takže vše je pet-friendly :).\n" +
                "\n" +
                "Ok, to by bylo super, můj e-mail je natalie.tercova@gmail.com :)\n" +
                "A jinak, jelikož máme podkrovní byt, máme spíše okna střešní :)\n";

        RulesMatcher rulesMatcher = new RulesMatcher();
        TreeSet<NerEntity> ruleMatchesByClass = rulesMatcher.getRuleMatchesByClass(email);
        Assert.assertEquals(1, ruleMatchesByClass.size());
        System.out.println(ruleMatchesByClass.first().getType());

    }
}
