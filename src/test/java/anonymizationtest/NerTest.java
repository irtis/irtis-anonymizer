package anonymizationtest;

import anonymization.pde.PdeEntity;
import nametag.NerEntity;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class NerTest {

    @Test
    public void flatten() {
        String input = "Moje jméno je Ondřej Sotolář.";

        NerEntity n1 = new NerEntity(14, 19);
        NerEntity n2 = new NerEntity(21, 28);
        NerEntity ident = new NerEntity(10, 12);
        PdeEntity toAnonymize = new PdeEntity();
        toAnonymize.setIdentifier(ident);
        toAnonymize.setQids(Arrays.asList(n1, n2));

        Assert.assertEquals(10, toAnonymize.flatten().getStart());
        Assert.assertEquals(28, toAnonymize.flatten().getEnd());
    }

    @Test
    public void getContent() {
        String input = "Moje jméno je Ondřej Sotolář.";
        NerEntity n1 = new NerEntity(14, 28);

        Assert.assertEquals("Ondřej Sotolář", n1.getContent(input));
    }
}
