package nametagtest;

import brat.BratEntity;
import brat.StandoffFile;
import brat.StandoffReader;
import html.HtmlGenerator;
import nametag.*;
import org.junit.Assert;
import org.junit.Test;
import rulematch.RulesMatcher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class BratNerMatchTest {
    private NameTagWrapper nameTagWrapper = new NameTagWrapper();
    private ResultReader resultReader = new ResultReader();
    private StandoffReader standoffReader = new StandoffReader();
    private EntityMatcher entityMatcher = new EntityMatcher();
    private HtmlGenerator htmlGenerator = new HtmlGenerator();
    private RulesMatcher rulesMatcher = new RulesMatcher();

    @Test
    public void evaluateMatchSuccess_naive() throws InterruptedException, IOException, NameTagException {
        final Path test1_ner_annotated = Paths.get("src", "test", "resources", "test1-ner-annotated.txt");
        final Path test1_ner_annotated_clean = Paths.get("src", "test", "resources", "test1-ner-annotated-clean.txt");
        final Path test1_plaintext = Paths.get("src", "test", "resources", "test1-plaintext.txt");
        final Path test1_brat_annotated = Paths.get("src", "test", "resources", "test1-brat-annotated.txt");
        final Path test1_html = Paths.get("src", "test", "resources", "test1-result.html");

        NameTagWrapper nameTagWrapper = new NameTagWrapper();
        nameTagWrapper.produceNametagNerAnnotatedFile(test1_plaintext, test1_ner_annotated);
        Assert.assertTrue(Files.exists(test1_ner_annotated));

        StringReplacer stringReplacer = new StringReplacer();
        stringReplacer.cleanStringFile(test1_ner_annotated, test1_ner_annotated_clean);
        Assert.assertTrue(Files.exists(test1_ner_annotated_clean));

        ResultReader resultReader = new ResultReader();
        String fileContent = Files.readString(test1_plaintext).replaceAll("\\r\\n", "\n");
        NerScanResult pureNerScanResult = resultReader.extractNerEntities(test1_ner_annotated_clean, "");

        RulesMatcher rulesMatcher = new RulesMatcher();
        NerScanResult nerScanResult = rulesMatcher.addRuleMatches(pureNerScanResult, fileContent);

        StandoffReader standoffReader = new StandoffReader();
        StandoffFile standoffFile = standoffReader.extractBratEntities(test1_brat_annotated, "");

        EntityMatcher entityMatcher = new EntityMatcher();
//        Map<BratEntity, Float> entityMap =
//                entityMatcher.combinedOverlapThreshold(
//                        standoffFile.getEntities(), nerScanResult.getNerEntities(), 50);

        Set<BratEntity> goldSet = new HashSet<>(standoffFile.getEntities());
        HtmlGenerator htmlGenerator = new HtmlGenerator();
        //float precision = entityMatcher.getPrecision(goldSet, entityMap.keySet());
//        float recall = entityMatcher.getRecall(goldSet, entityMap.keySet());
//        float fmeasure = entityMatcher.getFmeasure(goldSet, entityMap.keySet());

//        Files.write(test1_html, htmlGenerator.getHtmlResultPage(standoffFile.getEntities(),
//                nerScanResult.getNerEntities(), fileContent, test1_plaintext.getFileName().toString(), precision, recall, fmeasure).getBytes());
//
//        Assert.assertEquals(1, precision, 0.05);
//        Assert.assertEquals(1, recall, 0.05);
//        Assert.assertEquals(1, fmeasure, 0.05);
    }

    @Test
    public void evaluateMatchSuccess_ondraMartin() throws InterruptedException, IOException, NameTagException {
        final Path ner_annotated = Paths.get("src", "test", "resources", "test2-ner-annotated.txt");
        final Path plaintext = Paths.get("src", "test", "resources", "test2-plaintext.txt");
        final Path brat_annotated = Paths.get("src", "test", "resources", "test2-brat-annotated.ann");
        final Path html_result = Paths.get("src", "test", "resources", "test2-result.html");

        String fileContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");

        nameTagWrapper.produceNametagNerAnnotatedFile(plaintext, ner_annotated);
        NerScanResult nerScanResult = resultReader.extractNerEntities(ner_annotated, "");
        rulesMatcher.addRuleMatches(nerScanResult, fileContent);
        Assert.assertTrue(nerScanResult.getNerEntities().stream().allMatch(e -> entityMatcher.isAllowedNerClass(e.getType())));

        StandoffFile standoffFile = standoffReader.extractBratEntities(brat_annotated, "");
        Set<BratEntity> goldSet = new HashSet<>(standoffFile.getEntities());

        Map<BratEntity, List<NerEntity>> entityMap =
                entityMatcher.combinedOverlapThreshold(
                        standoffFile.getEntities(), nerScanResult.getNerEntities());

        float precision = entityMatcher.getPrecision(entityMap, nerScanResult.getNerEntities());
        float recall = entityMatcher.getRecall(entityMap);
        float fmeasure = entityMatcher.getFmeasure(entityMap, nerScanResult.getNerEntities());

        Files.write(html_result, htmlGenerator.getHtmlResultPage(standoffFile.getEntities(),
                nerScanResult.getNerEntities(), fileContent, plaintext.getFileName().toString(),
                precision, recall, fmeasure).getBytes());

        Assert.assertTrue(Files.exists(html_result));
    }

    @Test
    public void testOverlap() throws NameTagException {
        BratEntity be = new BratEntity();
        be.setStartIndex(10);
        be.setEndIndex(20);
        be.setText("aaaaaaaaaa");

        NerEntity ne1 = new NerEntity("<ne type=\"P\">", 0, 5);
        NerEntity ne2 = new NerEntity("<ne type=\"P\">", 0, 10);
        NerEntity ne3 = new NerEntity("<ne type=\"P\">", 5, 15);
        NerEntity ne4 = new NerEntity("<ne type=\"P\">", 15, 17);
        NerEntity ne5 = new NerEntity("<ne type=\"P\">", 15, 25);
        NerEntity ne6 = new NerEntity("<ne type=\"P\">", 20, 30);
        NerEntity ne7 = new NerEntity("<ne type=\"P\">", 25, 35);
        NerEntity ne8 = new NerEntity("<ne type=\"P\">", 5, 35);

        EntityMatcher em = new EntityMatcher();
        Assert.assertTrue(em.getEntitiesOverlap(be, ne1) == 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne2) == 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne3) > 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne4) > 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne5) > 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne6) == 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne7) == 0);
        Assert.assertTrue(em.getEntitiesOverlap(be, ne8) > 0);
    }

    @Test
    public void testMetrics_PartialOverlap() {
        EntityMatcher entityMatcher = new EntityMatcher();

        BratEntity b1 = new BratEntity();
        b1.setStartIndex(5);
        b1.setEndIndex(15);
        b1.setText("aaaaaaaaaa");
        List<BratEntity> bratEntities = new ArrayList<>();
        bratEntities.add(b1);

        NerEntity n1 = new NerEntity(3,8);
        NerEntity n3 = new NerEntity(9,11);
        NerEntity n4 = new NerEntity(9,14);
        NerEntity n2 = new NerEntity(13,20);
        List<NerEntity> nerEntities = new ArrayList<>();
        nerEntities.add(n1);
        nerEntities.add(n2);
        nerEntities.add(n3);
        nerEntities.add(n4);

        Map<BratEntity, List<NerEntity>> bratEntityListMap =
                entityMatcher.combinedOverlapThreshold(bratEntities, nerEntities);

        Assert.assertEquals(1f, entityMatcher.getPrecision(bratEntityListMap, nerEntities), 0.05);
        Assert.assertEquals(1f, entityMatcher.getRecall(bratEntityListMap), 0.05);
        Assert.assertEquals(1f, entityMatcher.getFmeasure(bratEntityListMap, nerEntities), 0.05);
    }

    @Test
    public void testMetrics_NoOverlap() {
        EntityMatcher entityMatcher = new EntityMatcher();

        BratEntity b1 = new BratEntity();
        b1.setStartIndex(5);
        b1.setEndIndex(15);
        b1.setText("aaaaaaaaaa");
        List<BratEntity> bratEntities = new ArrayList<>();
        bratEntities.add(b1);

        NerEntity n1 = new NerEntity(3,5);
        NerEntity n2 = new NerEntity(15,20);
        List<NerEntity> nerEntities = new ArrayList<>();
        nerEntities.add(n1);
        nerEntities.add(n2);

        Map<BratEntity, List<NerEntity>> bratEntityListMap =
                entityMatcher.combinedOverlapThreshold(bratEntities, nerEntities);

        Assert.assertEquals(0f, entityMatcher.getPrecision(bratEntityListMap, nerEntities), 0.05);
        Assert.assertEquals(0f, entityMatcher.getRecall(bratEntityListMap), 0.05);
    }

    @Test
    public void testMetrics_PartialMatch() {
        EntityMatcher entityMatcher = new EntityMatcher();

        BratEntity b1 = new BratEntity();
        b1.setStartIndex(5);
        b1.setEndIndex(15);
        b1.setText("aaaaaaaaaa");
        BratEntity b2 = new BratEntity();
        b2.setStartIndex(20);
        b2.setEndIndex(30);
        b2.setText("aaaaaaaaaa");
        List<BratEntity> bratEntities = new ArrayList<>();
        bratEntities.add(b1);
        bratEntities.add(b2);

        NerEntity n4 = new NerEntity(9,14);
        List<NerEntity> nerEntities = new ArrayList<>();
        nerEntities.add(n4);

        Map<BratEntity, List<NerEntity>> bratEntityListMap =
                entityMatcher.combinedOverlapThreshold(bratEntities, nerEntities);

        Assert.assertEquals(1f, entityMatcher.getPrecision(bratEntityListMap, nerEntities), 0.05);
        Assert.assertEquals(0.5, entityMatcher.getRecall(bratEntityListMap), 0.05);
        Assert.assertEquals(0.66, entityMatcher.getFmeasure(bratEntityListMap, nerEntities), 0.05);
    }

    @Test
    public void printWeightedResult() {
        int linesOfText = 4541 + 4265 + 639 + 829 + 547;
        int whole = 23 + 3 + 10 + 18 + 8;
        double weighted = ((23*0.83) + 3 + 10  + (18*0.72) + 8) / whole;

        System.out.println(String.format("\nLines of text:\t\t%d\nPI anonymization.entities:\t\t%d\nWeighted recall:\t%.2f\n", linesOfText, whole, weighted));
    }

}
