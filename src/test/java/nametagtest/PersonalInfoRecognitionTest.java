package nametagtest;

import nametag.*;
import org.junit.Assert;
import org.junit.Test;
import run.AggregateRunner;

import java.io.IOException;

public class PersonalInfoRecognitionTest {
    @Test
    public void aggregateResultsTest() throws IOException, InterruptedException, NameTagException {
        AggregateRunner runner = new AggregateRunner();
        float totalRecall = runner.getAggregateRecall();
        Assert.fail(String.format("Total recall: %.2f", totalRecall));
    }
}
