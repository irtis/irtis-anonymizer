package nametagtest;

import messenger.ConversationLoader;
import messenger.json.Conversation;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NameTagTest {
    @Test
    public void annotatePlainFile() throws IOException, InterruptedException {
        Path messengerJson = Paths.get("src", "test", "resources", "message_1.json");
        Path resultFile = Paths.get("src", "test", "resources", "resultfile.txt");
        Path intermediateFile = Paths.get("src", "test", "resources", "intermediatefile.txt");

        ConversationLoader fl = new ConversationLoader();
        fl.createAnnotatedPlainTextFile(messengerJson, resultFile, intermediateFile);
    }

    @Test
    public void annotateInputs() throws IOException {
        Path sourceFolder = Paths.get("src", "test", "resources", "testfile.txt");

        ConversationLoader fl = new ConversationLoader();
        Conversation conversation = fl.getConversationFromFile(sourceFolder, false);
        String toTag = conversation.toString();
        Path intermediatePath = Paths.get(sourceFolder.toString(), "intermediateConversation.txt");
        fl.saveToFile(intermediatePath, toTag);
    }
}