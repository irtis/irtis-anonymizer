package integrationtest;

import anonymization.CompositeDataAnonymizer;
import anonymization.DataAnonymizer;
import gazetteer.exceptions.GazetteerException;
import org.junit.Assert;
import org.junit.Test;
import server.entities.phone.PhoneCallEntity;

import java.io.IOException;

public class CompositeAnonymizerTest {

    // TODO: generate
    private PhoneCallEntity getPhoneCallEntity() {
        PhoneCallEntity e = new PhoneCallEntity();
        e.setName("Ondřej Sotolář");
        e.setPhoneNumber("+420 602 321 123");
        return e;
    }

    @Test
    public void anonymizePhoneCall() throws IOException, GazetteerException {
        DataAnonymizer anonymizer = new CompositeDataAnonymizer(2,20,4,4, 0, true);

        PhoneCallEntity entity = getPhoneCallEntity();
        PhoneCallEntity copy = getPhoneCallEntity();
        anonymizer.anonymize(entity);

        Assert.assertEquals(copy.getName(), entity.getName());
        Assert.assertEquals(copy.getPhoneNumber(), entity.getPhoneNumber());
    }
}
