package brattest;

import brat.StandoffFile;
import brat.StandoffReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class BratTest {
    @Test
    public void readStandoffFile() {
        Path bratSource = Paths.get("src", "test", "resources", "brat-source.txt");
        String bratResultName = "Test1";
        Path bratResult = Paths.get("src", "test", "resources", bratResultName + ".txt");

        StandoffReader standoffReader = new StandoffReader();
        StandoffFile standoffFile = standoffReader.extractBratEntities(bratResult, bratResultName);

        try(BufferedReader reader = Files.newBufferedReader(bratSource)){
            int position = 0;
            String currentLine = null;
            while ((currentLine = reader.readLine()) != null) {
                int linePosition = 0;
                while (linePosition < currentLine.length()) {
                    int adjustedPosition = standoffFile.getEntities().get(0).getStartIndex() - standoffFile.getNameOffset();
                    if (position == adjustedPosition) {
                        Assert.assertEquals(standoffFile.getEntities().get(0).getText(),
                                currentLine.substring(linePosition, linePosition + standoffFile.getEntities().get(0).getTextLength()));
                        return;
                    }
                    position++;
                    linePosition++;
                }
                position++;
            }

        } catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testSplit() {
        String line = "";
        String[] split = line.split("\\.");
        Assert.assertEquals(1, split.length);
    }
}
