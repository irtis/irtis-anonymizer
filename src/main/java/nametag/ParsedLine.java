package nametag;

import java.util.*;
import java.util.stream.Collectors;

public class ParsedLine {
    private String line;
    private List<NerEntity> entities;

    public ParsedLine() {
        this.line = "";
        this.entities = new ArrayList<>();
    }

    public ParsedLine(String line, List<NerEntity> entities) {
        this.line = line;
        this.entities = entities;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public List<NerEntity> getEntities() {
        return entities;
    }

    public void setEntities(List<NerEntity> entities) {
        this.entities = entities;
    }

    public String getEntityContent(NerEntity entity) {
        return line.substring(entity.getStart(), entity.getEnd());
    }

    public void appendToLine(String extra) {
        line += extra;
    }

    public void addEntity(NerEntity entity) {
        entities.add(entity);
    }

    public void merge(ParsedLine handleChunk) {
        if (handleChunk != null) {
            this.entities.addAll(handleChunk.getEntities());
        }
    }

    public NerEntity getLastEntity() {
        if (entities.size() > 0) {
            return entities.get(entities.size() - 1);
        } else {
            return null;
        }
    }

    public TreeSet<NerEntity> getSortedEntities(Set<String> filterTypes) {
        TreeSet<NerEntity> nerEntities = entities.stream()
                .filter(x -> filterTypes.contains(x.getType()))
                .collect(Collectors.toCollection(TreeSet::new));
        return nerEntities;
    }
}
