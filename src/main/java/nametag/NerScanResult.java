package nametag;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class NerScanResult {
    private String fileName;
    private List<NerEntity> nerEntities;
    private List<String> lines;

    public NerScanResult() {
        nerEntities = new ArrayList<>();
        lines = new ArrayList<>();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public List<NerEntity> getNerEntities() {
        return nerEntities;
    }

    public void setNerEntities(List<NerEntity> nerEntities) {
        this.nerEntities = nerEntities;
    }

    public List<String> getLines() {
        return lines;
    }

    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    public void addLine(ParsedLine parsedLine) {
        lines.add(parsedLine.getLine());
        nerEntities.addAll(parsedLine.getEntities());
    }
}
