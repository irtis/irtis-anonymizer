package nametag;

import java.util.Objects;

public class NerEntity implements Comparable<NerEntity> {
    // names
    public static final String TYPE_FULL_NAME = "P";
    public static final String TYPE_SURNAME = "ps";
    public static final String TYPE_FIRST_NAME = "pf";
    public static final String TYPE_SECOND_NAME = "pm";
    // id
    public static final String TYPE_CARD_PLATE = "rs";
    public static final String TYPE_PHONE_NUMBER = "rp";
    public static final String TYPE_BANK_ACCOUNT = "ra";
    public static final String TYPE_CREDIT_NUMBER = "rc";
    public static final String TYPE_BIRTH_NUMBER = "rb";
    public static final String TYPE_ID_CARD = "ro";
    // contact info
    public static final String TYPE_PHONE_NER = "at";
    public static final String TYPE_EMAIL = "re";
    // address
    public static final String TYPE_CITY = "gu";
    public static final String TYPE_STREET = "gs";
    public static final String TYPE_URBAN = "gq";
    public static final String TYPE_POSTAL = "az";
    public static final String TYPE_STREET_NO = "ah";
    public static final String TYPE_IPV4 = "a4";
    public static final String TYPE_GPS = "ag";

    private String type;
    private int start;
    private int end;

    public NerEntity() {
    }

    public NerEntity(int start, int end) {
        this.start = start;
        this.end = end;
        this.type = "regex";
    }

    public NerEntity(int start, int end, String type) {
        this.start = start;
        this.end = end;
        this.type = type;
    }

    /**
     * Constructor for NER tags of format <ne type="type">
     * @param startTag
     * @param start
     * @param end
     * @throws NameTagException
     */
    public NerEntity(String startTag, int start, int end) throws NameTagException {
        String[] split = startTag.split("\"");
        if (split.length != 3) throw new NameTagException("Tag inside not parsable: " + startTag);
        this.type = split[1];
        this.start = start;
        setEnd(end);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        if (end <= start) {
            throw new IllegalArgumentException("invalid interval: [" + start + "," + end + "]");
        }
        this.end = end;
    }

    public int getLength() {
        return end - start;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NerEntity nerEntity = (NerEntity) o;
        return start == nerEntity.start &&
                end == nerEntity.end &&
                Objects.equals(type, nerEntity.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, start, end);
    }

    @Override
    public int compareTo(NerEntity o) {
        if (this.getStart() < o.getStart()) return -1;
        if (this.getStart() > o.getStart()) return 1;
        // both start at the same index => longer is first
        if (this.getLength() > o.getLength()) return -1;
        if (this.getLength() < o.getLength()) return 1;
        return 0;
    }

    public String getContent(String target) {
        if (target == null
                || getStart() >= target.length()
                || getEnd() >= target.length()) {
            return target;
        }
        return target.substring(getStart(), getEnd());
    }

}
