package nametag;

public class NameTagException extends Exception {
    public NameTagException(String message) {
        super(message);
    }
}
