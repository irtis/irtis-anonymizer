package nametag;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Deprecated
public class ResultReader {

    private StringReplacer stringReplacer = new StringReplacer();
    private EntityMatcher entityMatcher = new EntityMatcher();
    /**
     * Parse NER xml file
     * @param path full path with filename
     * @param name
     * @return
     * @throws NameTagException
     */
    public NerScanResult readNerFile(Path path, String name) throws NameTagException {
        NerScanResult nerScanResult = new NerScanResult();
        int offset = 0;
        try (BufferedReader reader = Files.newBufferedReader(path)){
            String currentLine = null;
            while ((currentLine = reader.readLine()) != null) {
                ParsedLine parsedLine = readNerLine(currentLine, offset);
                if (parsedLine == null) {
                       throw new NameTagException("parsed line is null");
                }
                else {
                    nerScanResult.addLine(parsedLine);
                }
                offset += parsedLine.getLine().length() + 1;
            }
        } catch(IOException ex) {
            ex.printStackTrace();
        }
        return nerScanResult;
    }

    /**
     * Parse NER annotated file. Return NER anonymization.entities of allowed types and plaintext.
     * @param path
     * @param name offset in the length of given string (for Brat)
     * @return
     * @throws NameTagException
     * @throws IOException
     */
    public NerScanResult extractNerEntities(Path path, String name) throws NameTagException, IOException {
        NerScanResult nerScanResult = new NerScanResult();
        String fileContent = stringReplacer.replaceXmlEscape(Files.readString(path, StandardCharsets.UTF_8)
                .replaceAll("\\r\\n", "\n"));

        int nameOffset = name != null && !name.equals("")
                ? name.length() + 1 : 0; // +1 for line break

        ParsedLine parsedLine = readNerLine(fileContent, nameOffset);
        nerScanResult.addLine(parsedLine);
        return nerScanResult;
    }

    public ParsedLine readNerLine(String currentLine, int offset) throws NameTagException {
        String[] tokens = tokenize(currentLine);
        ParsedLine result = handleChunk(tokens, 0, tokens.length - 1, offset);
        List<NerEntity> filtered = new ArrayList<>();
        for (int i = 1; i < result.getEntities().size(); i++) {
            if (isValidSurname(result.getEntities().get(i-1), result.getEntities().get(i))){
                filtered.add(result.getEntities().get(i));
            }
        }
        result.setEntities(filtered);
        result.setLine(extractPlaintext(tokens));
        return result;
    }

    public String extractPlaintext(String[] tokens) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < tokens.length; i++) {
            if (!isTag(tokens[i])) {
                stringBuilder.append(tokens[i]);
            }
        }
        return stringBuilder.toString();
    }

    public ParsedLine handleChunk(String[] tokens, int start, int end, int offset) throws NameTagException {
        if (start > end || start >= tokens.length || end >= tokens.length) {
            return new ParsedLine();
        }
        int nextStartTag = findNextStartTag(tokens, start, end);
        if (nextStartTag < 0) {
            return new ParsedLine();
        }
        ParsedLine result = new ParsedLine();
        TagIndex endTag = findEndTag(tokens, nextStartTag, end);
        if (endTag == null) {
            throw new NameTagException("end tag not found.");
        }
        NerEntity entity = new NerEntity(
                tokens[nextStartTag],
                calcTagRealPosition(tokens, nextStartTag, offset),
                calcTagRealPosition(tokens, endTag.getIndex(), offset));

        if (isValidEntity(entity, result)) {
            result.addEntity(entity);
        }
        result.merge(handleChunk(tokens, nextStartTag + 1, endTag.getIndex() - 1, offset));
        result.merge(handleChunk(tokens, endTag.getIndex() + 1, end, offset));
        return result;
    }

    private boolean isValidSurname(NerEntity prev, NerEntity entity) {
        boolean isValidSurname =
                prev == null
                || !"ps".equals(entity.getType())
                || (isPreviousEntityName(prev.getType())
                && isPreviousEntityClose(entity, prev, 20));
        return isValidSurname;
    }

    private boolean isValidEntity(NerEntity entity, ParsedLine result) {
        return entityMatcher.isAllowedNerClass(entity.getType());
    }

    private boolean isPreviousEntityClose(NerEntity entity, NerEntity prev, int distance) {
        return entity.getStart() - prev.getEnd() < distance;
    }

    private boolean isPreviousEntityName(String entityType) {
        return "pf".equals(entityType)
                || "pm".equals(entityType)
                || "p_".equals(entityType);
    }

    public TagIndex findEndTag(String[] tokens, int startTagIndex) throws NameTagException {
        return findEndTag(tokens, startTagIndex, tokens.length -1);
    }

    public TagIndex findEndTag(String[] tokens, int startTagIndex, int end) throws NameTagException {
        int tagCount = 1;
        TagIndex nextTag = new TagIndex(startTagIndex, true);
        while (tagCount > 0 && nextTag.getIndex() < tokens.length) {
            nextTag = findNextTag(tokens, nextTag.getIndex() + 1, end);
            if (nextTag == null) {
                throw new NameTagException("end tag is null: " + startTagIndex + " " + Arrays.toString(tokens));
            }

            if (nextTag.isStart()) {
                tagCount++;
            } else {
                tagCount--;
            }
        }
        if (tagCount != 0) {
            throw new NameTagException("No end tag for: " + tokens[startTagIndex]);
        }
        return nextTag;
    }

    public int findNextStartTag(String[] tokens, int startAt) throws NameTagException {
        return findNextStartTag(tokens, startAt, tokens.length -1);
    }

    /**
     * Find next start tag. Inclusive bounds
     * @param tokens
     * @param startAt inclusive
     * @param endAt inclusive
     * @return
     * @throws NameTagException
     */
    public int findNextStartTag(String[] tokens, int startAt, int endAt) throws NameTagException {
        TagIndex nextTag = findNextTag(tokens, startAt, endAt);
        if (nextTag == null) {
            return -1;
        }
        if (!nextTag.isStart()) {
            throw new NameTagException("End tag before start tag.");
        }
        return nextTag.getIndex();
    }

    public TagIndex findNextTag(String[] tokens, int startAt) throws NameTagException {
        return findNextTag(tokens, startAt, tokens.length -1);
    }

    public TagIndex findNextTag(String[] tokens, int startAt, int endAt) throws NameTagException {
        if (endAt > tokens.length - 1) {
            throw new NameTagException("endAt > tokens.length");
        }
        for (int i = startAt; i <= endAt; i++) {
            if (isStartTag(tokens[i])) {
                return new TagIndex(i, true);
            } else if (isEndTag(tokens[i])) {
                return new TagIndex(i, false);
            }
        }
        return null;
    }

    public boolean isStartTag(String token) {
        return token.matches("<ne type=\"\\w{1,2}\">");
    }

    public boolean isEndTag(String token) {
        return token.matches("</ne>");
    }

    public boolean isTag(String token) {
        return token.matches("<ne type=\"\\w{1,2}\">|</ne>");
    }

    public int calcTagRealPosition(String[] tokens, int tagIndex, int offset) throws NameTagException {
        if (!isTag(tokens[tagIndex])) {
            throw new NameTagException("'" + tokens[tagIndex] + "' is not a tag.");
        }
        int total = 0;
        for (int i = 0; i < tagIndex; i++) {
            if (!isTag(tokens[i])) {
                total += tokens[i].length();
            }
        }
        return total + offset;
    }

    public String[] tokenize(String line) {
        return line.split("((?<=<ne type=\"\\w{1,2}\">|</ne>)|(?=<ne type=\"\\w{1,2}\">|</ne>))");
    }
}
