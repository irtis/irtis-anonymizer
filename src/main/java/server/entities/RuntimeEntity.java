package server.entities;

public class RuntimeEntity {
    private MetricIdentityEntity metricIdentity;
    private Long started;
    private Long stopped;
    private Integer type;

    public RuntimeEntity() {
    }

    public RuntimeEntity(MetricIdentityEntity metricIdentity, Long started, Long stopped, Integer type) {
        this.metricIdentity = metricIdentity;
        this.started = started;
        this.stopped = stopped;
        this.type = type;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public void setStarted(Long started) {
        this.started = started;
    }

    public void setStopped(Long stopped) {
        this.stopped = stopped;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public Long getStarted() {
        return started;
    }

    public Long getStopped() {
        return stopped;
    }

    public Integer getType() {
        return type;
    }
}
