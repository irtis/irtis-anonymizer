package server.entities;

public class LocationEntity {
    private MetricIdentityEntity metricIdentity;
    private Double latitude;
    private Double longitude;

    public LocationEntity() {
    }

    public LocationEntity(MetricIdentityEntity metricIdentity, Double latitude, Double longitude) {
        this.metricIdentity = metricIdentity;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
