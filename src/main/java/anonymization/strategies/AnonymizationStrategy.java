package anonymization.strategies;

public interface AnonymizationStrategy {
    String anonymize(String value);
}
