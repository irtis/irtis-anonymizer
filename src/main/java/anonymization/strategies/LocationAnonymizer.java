package anonymization.strategies;

import gazetteer.CityGazetteer;
import gazetteer.Mode;
import gazetteer.StreetGazetteer;
import gazetteer.exceptions.GazetteerException;
import logger.Logger;
import nametag.NerEntity;

import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Anonymize location.
 */
public class LocationAnonymizer {
    private static String DEFAULT_VALUE = "LOCATION";
    private final String DEFAULT_IP = "IP_ADDRESS";
    private final String DEFAULT_GPS = "GPS_COORDS";
    private final String IP_TAIL = ".0.0.0";

    // e.g.: 41°24’12.2″N   2°10’26.5″E
    private final String GPS_1 = "\\b\\d{1,3}°\\d{1,3}’\\d{1,3}\\.\\d{1,3}.{1,3}N.{1,3}\\d{1,3}°\\d{1,3}’\\d{1,3}\\.\\d{1,3}.{1,3}E\\b";
    private final Pattern gpsRegex_1 = Pattern.compile(GPS_1);
    private final String GPS_1_TAIL = "°0’0.0″";

    // e.g.: 41 24.2028,  2 10.4418
    private final String GPS_2 = "\\b\\d{1,3}\\s{1,3}\\d{1,3}\\.\\d{3,6}.{1,3}\\b\\d{1,3}\\s{1,3}\\d{1,3}\\.\\d{3,6}\\b";
    private final Pattern gpsRegex_2 = Pattern.compile(GPS_2);
    private final String GPS_2_TAIL = " 00.0000";

    // e.g.: 41.40338,  2.17403
    private final String GPS_3 = "\\b\\d{1,3}\\.\\d{3,6}.{1,3}\\d{1,3}\\.\\d{3,6}\\b";
    private final Pattern gpsRegex_3 = Pattern.compile(GPS_3);
    private final String GPS_3_TAIL = ".00000";

    private StreetGazetteer streetGazetteer = new StreetGazetteer();
    private CityGazetteer cityGazetteer = new CityGazetteer(Mode.BEST_EFFORT);
    private ReplacementGenerator replacementGenerator = new ReplacementGenerator();

    public LocationAnonymizer() throws IOException {
    }

    /**
     * Anonymize target according to the NER entity type.
     * @param value target
     * @param nerType NER type
     * @return anonymized value
     */
    public String anonymize(String value, String nerType) {
        String result = DEFAULT_VALUE;
        switch(nerType) {
            case NerEntity.TYPE_CITY:
                try {
                    result = cityGazetteer.getReplacement(value);;
                } catch (GazetteerException e) {
                    Logger.info("cityGazetteer failed on: ", value);
                }
                break;
            case NerEntity.TYPE_STREET:
            case NerEntity.TYPE_URBAN:
                result = streetGazetteer.getReplacement(value);
                break;
            case NerEntity.TYPE_POSTAL:
                result = replacePostalCode(value);
                break;
            case NerEntity.TYPE_STREET_NO:
                result = replaceStreetNo(value);
                break;
            case NerEntity.TYPE_IPV4:
                result = replaceIpv4(value);
                break;
            case NerEntity.TYPE_GPS:
                result = replaceGps(value);
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * Hash value of input. Keeps the Czech format: ddd dd.
     * @param value
     * @return
     */
    private String replacePostalCode(String value) {
        int intHash = replacementGenerator.getIntHash(value, 100000);
        while (intHash < 10000) {
            intHash *= 10;
        }
        String code = String.valueOf(intHash);
        return code.substring(0,3) + " " + code.substring(3);
    }

    /**
     * Hash value of input. Format: dd or ddd
     * @param value
     * @return
     */
    private String replaceStreetNo(String value) {
        int intHash = replacementGenerator.getIntHash(value, 1000);
        while (intHash < 10) {
            intHash *= 10;
        }
        return String.valueOf(intHash);
    }

    /**
     * Keep first address part and fill with trailing zeros.
     * @param value
     * @return
     */
    private String replaceIpv4(String value) {
        String[] split = value.split("\\.");
        if (split.length < 4) {
            return DEFAULT_IP;
        }
        return split[0] + IP_TAIL;
    }

    /**
     * Keep first address part and fill with trailing zeros.
     * @param value
     * @return
     */
    private String replaceGps(String value) {
        String result = DEFAULT_GPS;

        if (gpsRegex_1.matcher(value).matches()) {
            String[] split = value.split("°");
            if (split.length == 3) {
                result = split[0] + GPS_1_TAIL +"N "
                        + split[1].substring(split[1].length() - 2) + GPS_1_TAIL + "E";
            }

        } else if (gpsRegex_2.matcher(value).matches()) {
            String[] split = value.split(",");
            if (split.length == 2) {
                result = split[0].substring(0,2) + GPS_2_TAIL + ","
                        + split[1].substring(1,3) + GPS_2_TAIL;
            }

        } else if (gpsRegex_3.matcher(value).matches()) {
            String[] split = value.split(",");
            if (split.length == 2) {
                String[] periodSplit1 = split[0].split("\\.");
                String[] periodSplit2 = split[1].split("\\.");
                if (periodSplit1.length == 2 && periodSplit2.length == 2) {
                    result = periodSplit1[0] + GPS_3_TAIL + ", " + periodSplit2[0] + GPS_3_TAIL;
                }
            }
        }
        return result;
    }

}
