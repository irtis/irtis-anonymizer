package anonymization.strategies;

import gazetteer.FullNameGazetteer;
import gazetteer.Mode;
import gazetteer.exceptions.GazetteerException;
import logger.Logger;

import java.io.IOException;

/**
 * Class for anonymizing full names.
 */
public class FullNameAnonymizer implements AnonymizationStrategy {
    private static String DEFAULT_VALUE = "NAME";
    private FullNameGazetteer fullNameGazetteer = new FullNameGazetteer(Mode.BEST_EFFORT);

    /**
     * Constructor. Throws exception if FullNameGazetteer datafile is not in the root folder.
     * @throws IOException
     */
    public FullNameAnonymizer() throws IOException {
    }

    /**
     * Anonymize full name. Supports repeatability as long as the FullNameGazetteer datafile or salt is not changed.
     * @param name
     * @return
     */
    public String anonymize(String name) {
        try {
            return fullNameGazetteer.getReplacement(name);
        } catch (GazetteerException e) {
            Logger.info("NameStrategy failed on: ", name);
            return DEFAULT_VALUE;
        }
    }
}
