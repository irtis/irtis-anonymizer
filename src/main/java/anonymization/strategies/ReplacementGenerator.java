package anonymization.strategies;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

/**
 * Generate random and hashes values.
 */
public class ReplacementGenerator {
    Random random = new Random();

    /**
     * Random numerical string of specified length. Does not contain leading zeros.
     * @param digits lenght
     * @return numerical string
     */
    public String generateRandomNumericalString(int digits) {
        StringBuilder sb = new StringBuilder();
        sb.append(getRandomInt(1,9));
        for (int i = 1; i < digits; i++) {
            sb.append(getRandomInt(0, 9));
        }
        return sb.toString();
    }

    /**
     * Generate random int in range.
     * @param minInclusive
     * @param maxInclusive
     * @return
     */
    public int getRandomInt(int minInclusive, int maxInclusive) {
        if (minInclusive >= maxInclusive) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        return random.nextInt((maxInclusive - minInclusive) + 1) + minInclusive;
    }

    /**
     * Get int value of hash of the input, modulo the max bound.
     * @param input target string
     * @param maxExclusive max bound
     * @return
     */
    public int getIntHash(String input, int maxExclusive) {
        byte[] hashedPassword = getHash(input);
        int hashCode = Arrays.hashCode(hashedPassword);
        int modHash = hashCode % maxExclusive;
        if (modHash < 0)  {
            modHash += maxExclusive;
        }
        return modHash;
    }

    private byte[] getHash(String input) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Wrong hashing algorithm name!");
        }
        byte[] hashedPassword = md.digest(input.getBytes());
        return hashedPassword;
    }
}
