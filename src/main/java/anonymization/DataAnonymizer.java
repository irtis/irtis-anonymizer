package anonymization;

import server.entities.Conversation;
import server.entities.GeneralText;
import server.entities.User;
import server.entities.phone.PhoneCallEntity;
import server.entities.phone.PhoneSmsEntity;

public interface DataAnonymizer {
    void anonymize(PhoneCallEntity entity);
    void anonymize(PhoneSmsEntity entity);
    void anonymize(Conversation entity);
    void anonymize(GeneralText entity);
    void anonymize(User entity);
}
