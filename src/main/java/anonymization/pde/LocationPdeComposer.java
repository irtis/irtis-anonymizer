package anonymization.pde;


import nametag.NerEntity;
import nametag.ParsedLine;

import java.util.*;

public class LocationPdeComposer extends PdeComposer {

    protected int orderCloseness = 3;
    protected int indexCloseness = 40;
    protected int minStreetQids = 0;


    public LocationPdeComposer() {
        this.pdeType = PdeEntity.TYPE_LOCATION;
        this.NER_TYPES = new HashSet<>(Arrays.asList(
                NerEntity.TYPE_CITY,
                NerEntity.TYPE_STREET,
                NerEntity.TYPE_URBAN,
                NerEntity.TYPE_POSTAL,
                NerEntity.TYPE_STREET_NO,
                NerEntity.TYPE_IPV4,
                NerEntity.TYPE_GPS));
//        this.QID_TYPES = Arrays.asList(
//                NerEntity.TYPE_CITY,
//                NerEntity.TYPE_STREET,
//                NerEntity.TYPE_URBAN,
//                NerEntity.TYPE_POSTAL,
//                NerEntity.TYPE_STREET_NO);
    }

//    @Override
//    public TreeSet<PdeEntity> compose(ParsedLine parsedLine) {
//        NerEntity[] sortedEntities = parsedLine.getSortedEntities(NER_TYPES).toArray(new NerEntity[0]);
//        Set<PdeEntity> pdeEntities = new HashSet<>();
//
//        for (int i = 0; i < sortedEntities.length; i++) {
//            if (sortedEntities[i].getContent(parsedLine.getLine()).length() < minIdentifierLength) {
//                continue;
//            }
//            // handle full names
//            if (NerEntity.TYPE_IPV4.equals(sortedEntities[i].getType())
//                    || NerEntity.TYPE_GPS.equals(sortedEntities[i].getType())) {
//                PdeEntity created = new PdeEntity(sortedEntities[i], PdeEntity.TYPE_LOCATION);
//                created.setTextContent(sortedEntities[i].getContent(parsedLine.getLine()));
//                pdeEntities.add(created);
//            }
//            // handle surnames
//            else if (NerEntity.TYPE_STREET.equals(sortedEntities[i].getType())
//                    || NerEntity.TYPE_STREET_NO.equals(sortedEntities[i].getType())
//                    || NerEntity.TYPE_CITY.equals(sortedEntities[i].getType())
//                    || NerEntity.TYPE_URBAN.equals(sortedEntities[i].getType())
//                    || NerEntity.TYPE_POSTAL.equals(sortedEntities[i].getType())
//            ) {
//                PdeEntity handled = handleStreet(sortedEntities, i, parsedLine);
//                if (handled.getQids().size() >= minStreetQids) {
//                    pdeEntities.add(handled);
//                }
//            }
//        }
//        return removeOverlaps(pdeEntities);
//    }
//
//    // TODO prioritize by distance, scan neighborhood both ways
//    private PdeEntity handleStreet(NerEntity[] entities, int identifier, ParsedLine parsedLine) {
//        PdeEntity result = new PdeEntity(entities[identifier], PdeEntity.TYPE_LOCATION);
//        String surname = entities[identifier].getContent(parsedLine.getLine());
//        List<String> firstNames = new ArrayList<>();
//        // scan neighborhood
//        int i = Math.max(0, identifier - orderCloseness);
//        int limit = Math.min(entities.length - 1, identifier + orderCloseness);
//        int idCenter = (entities[identifier].getEnd() + entities[identifier].getStart()) / 2;
//
//        for (;  i < limit; i++) {
//            if (i != identifier && QID_TYPES.contains(entities[i].getType())) {
//
//                int otherCenter = (entities[i].getEnd() + entities[i].getStart()) / 2;
//                int distance = Math.abs(otherCenter - idCenter);
//                if (distance < indexCloseness) {
//                    result.getQids().add(entities[i]);
//                    firstNames.add(entities[i].getContent(parsedLine.getLine()));
//                }
//            }
//        }
//        firstNames.add(surname);
//        result.setTextContent(String.join(" ", firstNames));
//        return result;
//    }
}
