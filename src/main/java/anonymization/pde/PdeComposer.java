package anonymization.pde;

import nametag.NerEntity;
import nametag.ParsedLine;

import java.util.*;

public abstract class PdeComposer {
    protected Set<String> NER_TYPES;
    protected int orderCloseness;
    protected int indexCloseness;
    protected int minIdentifierLength;
    protected List<String> QID_TYPES;
    protected String pdeType;

    public TreeSet<PdeEntity> compose(ParsedLine parsedLine) {
        NerEntity[] sortedEntities = parsedLine.getSortedEntities(NER_TYPES).toArray(new NerEntity[0]);
        Set<PdeEntity> pdeEntities = new HashSet<>();

        for (NerEntity n: sortedEntities) {
            pdeEntities.add(new PdeEntity(n, pdeType, n.getType(), parsedLine.getLine()));
        }
        return removeOverlaps(pdeEntities);
    }

    protected TreeSet<PdeEntity> removeOverlaps(Set<PdeEntity> pdeEntities) {
        return new TreeSet<>(pdeEntities);
    }

    /**
     *
     * @param sorted
     * @return
     */
    public static TreeSet<PdeEntity> linearize(TreeSet<PdeEntity> sorted) {
        if (sorted == null || sorted.size() < 1) {
            return sorted;
        }
        TreeSet<PdeEntity> clean = new TreeSet<>();

        PdeEntity current = sorted.first();
        clean.add(current);
        while (current != null) {
            current = sorted.higher(current);
            if (current == null) {
                break;
            }
            if (current.flatten().getStart() >= clean.last().flatten().getEnd()) {
                clean.add(current);
            }
        }
        return clean;
    }
}
