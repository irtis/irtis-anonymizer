package anonymization.pde;

import nametag.NerEntity;

import java.util.*;
import java.util.stream.Collectors;

public class PdeEntity implements Comparable<PdeEntity> {
    public static final String TYPE_NAME = "name";
    public static final String TYPE_ID = "id";
    public static final String TYPE_LOCATION = "location";
    public static final String TYPE_CONTACT_INFO = "contactinfo";
    private static final String TYPE_MERGED = "_MERGED_";

    private String type;
    private String nerType;
    private NerEntity identifier;
    private List<NerEntity> qids;
    private String textContent;

    public PdeEntity() {
    }

    public PdeEntity(List<PdeEntity> list) {
        TreeSet<NerEntity> sorted = list.stream()
                .map(PdeEntity::flatten)
                .collect(Collectors.toCollection(TreeSet::new));
        identifier = new NerEntity(sorted.first().getStart(), sorted.last().getEnd());
        this.qids = new ArrayList<>();
        this.type = TYPE_MERGED;
        this.textContent = TYPE_MERGED;
    }

    public PdeEntity(NerEntity identifier, String type) {
        this.identifier = identifier;
        this.qids = new ArrayList<>();
        this.type = type;
    }

    public PdeEntity(NerEntity identifier, String type, String nerType, String target) {
        this.identifier = identifier;
        this.qids = new ArrayList<>();
        this.type = type;
        this.nerType = nerType;
        this.textContent = identifier.getContent(target);
    }

    public PdeEntity(NerEntity identifier, List<NerEntity> qids) {
        this.identifier = identifier;
        this.qids = qids;
    }

    public NerEntity getIdentifier() {
        return identifier;
    }

    public void setIdentifier(NerEntity identifier) {
        this.identifier = identifier;
    }

    public List<NerEntity> getQids() {
        return qids;
    }

    public void setQids(List<NerEntity> qids) {
        this.qids = qids;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNerType() {
        return nerType;
    }

    public void setNerType(String nerType) {
        this.nerType = nerType;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public NerEntity flatten() {
        TreeSet<NerEntity> sorted = new TreeSet<>(qids);
        sorted.add(identifier);
        return new NerEntity(sorted.first().getStart(), sorted.last().getEnd());
    }

    @Override
    public int compareTo(PdeEntity o) {
        NerEntity thisOne = flatten();
        NerEntity other = o.flatten();
        return thisOne.compareTo(other);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PdeEntity)) return false;
        PdeEntity pdeEntity = (PdeEntity) o;
        return Objects.equals(getType(), pdeEntity.getType()) &&
                Objects.equals(getNerType(), pdeEntity.getNerType()) &&
                Objects.equals(getIdentifier(), pdeEntity.getIdentifier()) &&
                getQids().size() == pdeEntity.getQids().size() &&
                pdeEntity.getQids().containsAll(getQids());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getNerType(), getIdentifier(), getQids());
    }
}
