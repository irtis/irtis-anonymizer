package anonymization.pde;

import nametag.NerEntity;

import java.util.Arrays;
import java.util.HashSet;

public class ContactInfoPdeComposer extends PdeComposer {

    public ContactInfoPdeComposer() {
        this.pdeType = PdeEntity.TYPE_CONTACT_INFO;
        this.NER_TYPES = new HashSet<>(Arrays.asList(
                NerEntity.TYPE_PHONE_NER,
                NerEntity.TYPE_PHONE_NUMBER,
                NerEntity.TYPE_EMAIL));
    }
}
