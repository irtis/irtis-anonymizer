package anonymization;

import anonymization.strategies.*;
import gazetteer.exceptions.GazetteerException;
import nametag.NerEntity;
import server.entities.Conversation;
import server.entities.GeneralText;
import server.entities.Message;
import server.entities.User;
import server.entities.phone.PhoneCallEntity;
import server.entities.phone.PhoneSmsEntity;

import java.io.IOException;
import java.util.stream.Collectors;

public class CompositeDataAnonymizer implements DataAnonymizer {

    private FullNameAnonymizer fullNameAnonymizer;
    private IdAnonymizer idAnonymizer;
    private ContactInfoAnonymizer contactInfoAnonymizer;
    private LocationAnonymizer locationAnonymizer;
    private UnstructuredTextAnonymizer unstructuredTextAnonymizer;


    public CompositeDataAnonymizer(int orderCloseness, int indexCloseness, int minIdentifierLength, int minInputLength,
                                   int minFirstNames, boolean isUsingRules)
            throws IOException, GazetteerException {
        this.fullNameAnonymizer = new FullNameAnonymizer();
        this.idAnonymizer = new IdAnonymizer();
        this.contactInfoAnonymizer = new ContactInfoAnonymizer();
        this.locationAnonymizer = new LocationAnonymizer();
        this.unstructuredTextAnonymizer = new UnstructuredTextAnonymizer(
                fullNameAnonymizer, idAnonymizer, contactInfoAnonymizer, locationAnonymizer,
                orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);
    }

    @Override
    public void anonymize(PhoneCallEntity entity) {
        entity.setName(fullNameAnonymizer.anonymize(entity.getName()));
        entity.setPhoneNumber(contactInfoAnonymizer.anonymize(entity.getPhoneNumber(), NerEntity.TYPE_PHONE_NUMBER));
    }

    @Override
    public void anonymize(PhoneSmsEntity entity) {
        entity.setPhoneNumber(contactInfoAnonymizer.anonymize(entity.getPhoneNumber(), NerEntity.TYPE_PHONE_NUMBER));
        entity.setContent(unstructuredTextAnonymizer.anonymize(entity.getContent()));
    }

    @Override
    public void anonymize(Conversation entity) {
        entity.setFrom(fullNameAnonymizer.anonymize(entity.getFrom()));
        for (Message message: entity.getMessages()) {
            message.setFrom(fullNameAnonymizer.anonymize(message.getFrom()));
            message.setParticipants(message.getParticipants().stream()
                    .map(p -> fullNameAnonymizer.anonymize(p))
                    .collect(Collectors.toList()));
            message.setContent(unstructuredTextAnonymizer.anonymize(message.getContent()));
        }
    }

    @Override
    public void anonymize(GeneralText entity) {
        entity.setText(unstructuredTextAnonymizer.anonymize(entity.getText()));
    }

    @Override
    public void anonymize(User entity) {
        entity.setName(fullNameAnonymizer.anonymize(entity.getName()));
        entity.setEmail(contactInfoAnonymizer.anonymize(entity.getEmail(), NerEntity.TYPE_EMAIL));
        entity.setPhoneNumber(contactInfoAnonymizer.anonymize(entity.getPhoneNumber(), NerEntity.TYPE_PHONE_NUMBER));
    }
}
