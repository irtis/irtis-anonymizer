package datacleaner;

import java.util.HashSet;
import java.util.Random;

public class MobileNumberGenerator {
    private Random random = new Random();
    private final String[] CZECH_MOBILE_OPRATOR_PREFIX = new String[] {
            "601", "602", "607", "603", "604", "605", "608", "702", "720", "730", "739", "770", "777" };
    private final  String[] MOBILE_TRIPLETS = new String[] {
            "789", "456", "123", "234", "567", "147", "258", "369", "159", "357", "157", "268", "359", "248" };
    private final  String[] COUNTRY_PREFIX = new String[] { "+420 ", "" };
    private final  String[] COUNTRY_PREFIX_ALPH = new String[] { "420 ", "" };
    private final  String MOBILE_BASE = "%s%s %s %s";
    private HashSet<String> mobileNumbers;

    public MobileNumberGenerator() {
        this.mobileNumbers = new HashSet<>(COUNTRY_PREFIX_ALPH.length * CZECH_MOBILE_OPRATOR_PREFIX.length * MOBILE_TRIPLETS.length * MOBILE_TRIPLETS.length);
        initGazetteer();
    }

    private void initGazetteer() {
        for (String prefix: COUNTRY_PREFIX_ALPH) {
            for (String base: CZECH_MOBILE_OPRATOR_PREFIX) {
                for (String tr1: MOBILE_TRIPLETS) {
                    for (String tr2: MOBILE_TRIPLETS) {
                        mobileNumbers.add(String.format(MOBILE_BASE, prefix, base, tr1, tr2));
                    }
                }
            }
        }
    }

    public boolean contains(String value) {
        return mobileNumbers.contains(value);
    }

    public String generate() {
        return String.format(MOBILE_BASE,
                COUNTRY_PREFIX[random.nextInt(COUNTRY_PREFIX.length)],
                CZECH_MOBILE_OPRATOR_PREFIX[random.nextInt(CZECH_MOBILE_OPRATOR_PREFIX.length)],
                MOBILE_TRIPLETS[random.nextInt(MOBILE_TRIPLETS.length)],
                MOBILE_TRIPLETS[random.nextInt(MOBILE_TRIPLETS.length)]);
    }
}