package datacleaner;

public class SyntheticCleaner {
    MobileNumberGenerator mobileNumberGenerator = new MobileNumberGenerator();
    NameGenerator nameGenerator = new NameGenerator();

    public boolean contains(String value) {
        return nameGenerator.contains(value) || mobileNumberGenerator.contains(value);
    }
}
