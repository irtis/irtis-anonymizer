package messenger;

import com.fasterxml.jackson.databind.ObjectMapper;
import messenger.json.Conversation;
import messenger.json.Message;
import nametag.NameTagWrapper;
import nametag.StringReplacer;

import java.io.*;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConversationLoader {
    public static String MESSENGER_MESSAGES_FILENAME = "message_1.json";

    private StringReplacer stringReplacer = new StringReplacer();

    /**
     * Parse Mesenger messages from messages_1.txt
     * @param path path of the messages_1.txt file without the filename
     * @return
     * @throws IOException
     */
    public Conversation getConversationFromFile(Path path, boolean useDefaultFilename) throws IOException {

        String badlyEncoded = Files.readString(Paths.get(path.toString(),
                useDefaultFilename ? MESSENGER_MESSAGES_FILENAME : ""), StandardCharsets.UTF_8);
        String unescapeJava = unescapeMessenger(badlyEncoded);
        byte[] bytes = unescapeJava.getBytes(StandardCharsets.ISO_8859_1);
        String fixed = new String(bytes, StandardCharsets.UTF_8);

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(fixed, Conversation.class);
    }



    /**
     * Get messages from conversation in string. Every message is on a separate line.
     * @param conversation
     * @return
     */
    public String extractConversationContent(Conversation conversation) {
        StringBuilder sb = new StringBuilder();
        for (Message m : conversation.getMessages()) {
            sb.append(m.getContent());
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Path with filename
     * @param path
     */
    public void saveToFile(Path path, String text) throws IOException {
        Files.write(path, text.getBytes());
    }

    public void createAnnotatedPlainTextFile(Path messengerJson, Path resultFile, Path intermediateFile) throws IOException, InterruptedException {
        // Convert Messenger json file to new plain text file with conversation content
        String plainCnversation = getConversationFromFile(messengerJson, false).toString();
        saveToFile(intermediateFile, plainCnversation);

        // Annotate with NameTag
        NameTagWrapper wrapper = new NameTagWrapper();
        wrapper.produceNametagNerAnnotatedFile(intermediateFile, resultFile);

        // Remove useless xml tags
        stringReplacer.cleanStringFile(resultFile, resultFile);
    }

    /**
     * Convert all escaped unicode chars within string to utf-8 chars
     * @param str
     * @return
     */
    public String unescapeMessenger(String str) {
        if (str == null) {
            return null;
        }
        try {
            StringWriter writer = new StringWriter(str.length());
            unescapeMessenger(writer, str);
            return writer.toString();
        } catch (IOException ioe) {
            // this should never ever happen while writing to a StringWriter
            throw new RuntimeException(ioe);
        }
    }

    private void unescapeMessenger(Writer out, String str) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("The Writer must not be null");
        }
        if (str == null) {
            return;
        }
        int sz = str.length();
        StringBuilder unicode = new StringBuilder(4);
        boolean hadSlash = false;
        boolean inUnicode = false;
        for (int i = 0; i < sz; i++) {
            char ch = str.charAt(i);
            if (inUnicode) {
                unicode.append(ch);
                if (unicode.length() == 4) {
                    // unicode now contains the four hex digits
                    // which represents our unicode character
                    try {
                        int value = Integer.parseInt(unicode.toString(), 16);
                        out.write((char) value);
                        unicode.setLength(0);
                        inUnicode = false;
                        hadSlash = false;
                    } catch (NumberFormatException nfe) {
                        throw new RuntimeException("Unable to parse unicode value: " + unicode, nfe);
                    }
                }
                continue;
            }
            if (hadSlash) {
                hadSlash = false;
                if (ch == 'u') {
                    inUnicode = true;
                } else {
                    out.write("\\");
                    out.write(ch);
                }
                continue;
            } else if (ch == '\\') {
                hadSlash = true;
                continue;
            }
            out.write(ch);
        }
        if (hadSlash) {
            // then we're in the weird case of a \ at the end of the
            // string, let's output it anyway.
            out.write('\\');
        }
    }



}
