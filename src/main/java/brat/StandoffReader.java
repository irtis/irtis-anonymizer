package brat;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class StandoffReader {
    private static final int ID_LENGTH = 2;
    private static final String TAB = "\t";
    private static final String ENTITY = "T";

    /**
     * Parse Brat standoff file
     * @param path with filename
     * @param name filename itself
     * @return
     */
    public StandoffFile extractBratEntities(Path path, String name) {
        List<BratEntity> entities = new ArrayList<>();
        try(BufferedReader reader = Files.newBufferedReader(path)){
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                BratEntity bratEntity = parseLine(currentLine);
                if (bratEntity != null) {
                    entities.add(bratEntity);
                }
            }
        } catch(IOException | BratParserException ex) {
            ex.printStackTrace();
        }

        StandoffFile result = new StandoffFile();
        result.setEntities(entities);
        result.setName(name);
        return result;
    }

    private BratEntity parseLine(String currentLine) throws BratParserException {
        String[] split = currentLine.split(TAB);
        if (split[0].length() < 1) {
            return null;
        }
        if (ENTITY.equals(split[0].substring(0,1))) {
            if (split.length < 2 || split.length > 3)
                throw new BratParserException("Line: '" + currentLine + "' has wrong format.");

            BratEntity entity = new BratEntity();
            if (split.length == 3) {
                entity.setText(split[2]);
            }

            String[] range = split[1].split(" ");
            if (range.length != 3) throw new BratParserException("Line: '" + currentLine + "' has wrong format.");
            entity.setType(range[0]);
            entity.setStartIndex(Integer.parseInt(range[1]));
            entity.setEndIndex(Integer.parseInt(range[2]));

            return entity;
        }
        return null;
    }
}
