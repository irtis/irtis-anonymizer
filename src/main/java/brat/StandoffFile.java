package brat;

import java.util.List;

public class StandoffFile {
    private String name;
    private List<BratEntity> entities;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BratEntity> getEntities() {
        return entities;
    }

    public void setEntities(List<BratEntity> entities) {
        this.entities = entities;
    }

    public int getNameOffset() {
        return name.length() + 1;
    }
}
