package brat;

public class BratParserException extends Exception {
    public BratParserException(String message) {
        super(message);
    }
}
