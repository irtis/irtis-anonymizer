package run;

import anonymization.pde.PdeEntity;
import brat.BratEntity;
import nametag.EntityMatcher;

import java.util.ArrayList;
import java.util.List;

public class AnnotationResult {
    private float recall;
    private float precision;
    private float fmeasure;
    private List<BratEntity> gold = new ArrayList<>();
    private List<PdeEntity> retrieved = new ArrayList<>();
    private List<PdeEntity> hits = new ArrayList<>();
    private List<PdeEntity> misses = new ArrayList<>();
    private EntityMatcher entityMatcher;

    List<PdeEntity> gold_brat = new ArrayList<>();
    List<BratEntity> retrieved_brat = new ArrayList<>();
    List<BratEntity> hits_brat = new ArrayList<>();
    

    public AnnotationResult(float recall, float precision, float fmeasure) {
        this.recall = recall;
        this.precision = precision;
        this.fmeasure = fmeasure;
    }

    public AnnotationResult(EntityMatcher entityMatcher, float precision, float recall, List<BratEntity> gold,
                            List<PdeEntity> retrieved, List<PdeEntity> hits, List<PdeEntity> misses) {
        this.entityMatcher = entityMatcher;
        this.gold = gold;
        this.retrieved = retrieved;
        this.hits = hits;
        this.misses = misses;
        this.recall = recall;
        //calculateMetrics(precision, recall);
    }

    public AnnotationResult(float precision, float recall, List<PdeEntity> gold,
                            List<BratEntity> retrieved, List<BratEntity> hits) {
        this.entityMatcher = entityMatcher;
        this.gold_brat = gold;
        this.retrieved_brat = retrieved;
        this.hits_brat = hits;
        this.recall = recall;
        //calculateMetrics(precision, recall);
    }

    private void calculateMetrics(float precision, float recall) {
        this.precision = entityMatcher.addWeight(precision, retrieved.size());
        this.recall = entityMatcher.addWeight(recall, retrieved.size());
        this.fmeasure = entityMatcher.calculateWeightedFmeasure(precision, recall, retrieved.size());
    }

    public float getRecall() {
        return recall;
    }

    public void setRecall(float recall) {
        this.recall = recall;
    }

    public float getPrecision() {
        return precision;
    }

    public void setPrecision(float precision) {
        this.precision = precision;
    }

    public float getFmeasure() {
        return fmeasure;
    }

    public void setFmeasure(float fmeasure) {
        this.fmeasure = fmeasure;
    }

    public List<PdeEntity> getMisses() {
        return misses;
    }

    public void setMisses(List<PdeEntity> misses) {
        this.misses = misses;
    }

    public List<PdeEntity> getRetrieved() {
        return retrieved;
    }

    public void setRetrieved(List<PdeEntity> retrieved) {
        this.retrieved = retrieved;
    }

    public List<PdeEntity> getHits() {
        return hits;
    }

    public void setHits(List<PdeEntity> hits) {
        this.hits = hits;
    }

    public List<BratEntity> getGold() {
        return gold;
    }

    public void setGold(List<BratEntity> gold) {
        this.gold = gold;
    }
}
