package run;

import anonymization.CompositeDataAnonymizer;
import anonymization.pde.PdeEntity;
import anonymization.strategies.UnstructuredTextAnonymizer;
import brat.BratEntity;
import brat.StandoffFile;
import brat.StandoffReader;
import datacleaner.SyntheticCleaner;
import gazetteer.exceptions.GazetteerException;
import html.HtmlGenerator;
import nametag.*;
import rulematch.RulesMatcher;
import server.entities.GeneralText;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class AggregateRunner {private NameTagWrapper nameTagWrapper = new NameTagWrapper();
    private ResultReader resultReader = new ResultReader();
    private StandoffReader standoffReader = new StandoffReader();
    private EntityMatcher entityMatcher = new EntityMatcher();
    private HtmlGenerator htmlGenerator = new HtmlGenerator();
    private RulesMatcher rulesMatcher = new RulesMatcher();
    private SyntheticCleaner syntheticCleaner = new SyntheticCleaner();

    private Path messengerConversations = Paths.get("testdata", "messenger-conversations");
    private List<String> conversationNames = List.of(
            "mayhameggs",
            "vojtechbrezik",
            "lenkavavrusova",
            "karelvasak",
            "doriszelmanova");

    private String nerAnnotated = "-ner-annotated.txt";
    private String plaintextTxt = "_plaintext.txt";
    private String annFile = "_plaintext.ann";
    private String resultHtml = "-result.html";

    @Deprecated
    public float getAggregateRecall() throws IOException, NameTagException, InterruptedException {
        int totalEntities = 0;
        float totalRecall = 0f;

        for (String conversation: conversationNames) {
            final Path ner_annotated = messengerConversations.resolve(conversation).resolve(conversation + nerAnnotated);
            final Path plaintext = messengerConversations.resolve(conversation).resolve(conversation + plaintextTxt);
            final Path brat_annotated = messengerConversations.resolve(conversation).resolve(conversation + annFile);
            final Path html_result = messengerConversations.resolve(conversation).resolve(conversation + resultHtml);

            String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");

            nameTagWrapper.produceNametagNerAnnotatedFile(plaintext, ner_annotated);
            NerScanResult nerScanResult = resultReader.extractNerEntities(ner_annotated, "");
            rulesMatcher.addRuleMatches(nerScanResult, plaintextContent);

            StandoffFile standoffFile = standoffReader.extractBratEntities(brat_annotated, "");

            Map<BratEntity, List<NerEntity>> entityMap =
                    entityMatcher.combinedOverlapThreshold(
                            standoffFile.getEntities(), nerScanResult.getNerEntities());

            float precision = entityMatcher.getPrecision(entityMap, nerScanResult.getNerEntities());
            float recall = entityMatcher.getRecall(entityMap);
            float fmeasure = entityMatcher.getFmeasure(entityMap, nerScanResult.getNerEntities());

            Files.write(html_result, htmlGenerator.getHtmlResultPage(standoffFile.getEntities(),
                    nerScanResult.getNerEntities(), plaintextContent, plaintext.getFileName().toString(),
                    precision, recall, fmeasure).getBytes());

            totalEntities += standoffFile.getEntities().size();
            totalRecall += standoffFile.getEntities().size() * recall;
        }
        return totalRecall / totalEntities;
    }

    @Deprecated
    public float getAggregateRecallInMemory() throws IOException, GazetteerException {
        int totalEntities = 0;
        float totalRecall = 0f;
        NerInMemoryRecognizer nerInMemoryRecognizer = new NerInMemoryRecognizer();

        for (String conversation: conversationNames) {
            final Path plaintext = messengerConversations.resolve(conversation).resolve(conversation + plaintextTxt);
            final Path brat_annotated = messengerConversations.resolve(conversation).resolve(conversation + annFile);
            final Path html_result = messengerConversations.resolve(conversation).resolve(conversation + resultHtml);

            String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");
            ParsedLine parsedLine = nerInMemoryRecognizer.parseLine(plaintextContent);
            NerScanResult nerScanResult = new NerScanResult();
            nerScanResult.addLine(parsedLine);
            rulesMatcher.addRuleMatches(nerScanResult, plaintextContent);

            StandoffFile standoffFile = standoffReader.extractBratEntities(brat_annotated, "");

            Map<BratEntity, List<NerEntity>> entityMap =
                    entityMatcher.combinedOverlapThreshold(
                            standoffFile.getEntities(), nerScanResult.getNerEntities());

            float precision = entityMatcher.getPrecision(entityMap, nerScanResult.getNerEntities());
            float recall = entityMatcher.getRecall(entityMap);
            float fmeasure = entityMatcher.getFmeasure(entityMap, nerScanResult.getNerEntities());

            Files.write(html_result, htmlGenerator.getHtmlResultPage(standoffFile.getEntities(),
                    nerScanResult.getNerEntities(), plaintextContent, plaintext.getFileName().toString(),
                    precision, recall, fmeasure).getBytes());

            totalEntities += standoffFile.getEntities().size();
            totalRecall += standoffFile.getEntities().size() * recall;
        }
        return totalRecall / totalEntities;
    }

    public String measureAnonThroughput() throws IOException, GazetteerException {
        int orderCloseness = 1;
        int indexCloseness = 30;
        int minIdentifierLength = 5;
        int minInputLength = 4;
        int minFirstNames = 0;
        boolean isUsingRules = true;

        CompositeDataAnonymizer anonymizer = new CompositeDataAnonymizer(orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);

        long startTime = System.nanoTime();
        Path studentData = Paths.get("testdata", "students");
        List<Path> studentFolders = Files
                .walk(studentData, 1)
                .filter(f -> !studentData.equals(f))
                .filter(Files::isDirectory)
                .collect(Collectors.toList());

        for (Path studentFolder : studentFolders) {
            List<Path> annFiles = Files.walk(studentFolder, 1)
                    .filter(f -> !studentFolder.equals(f))
                    .filter(Files::isRegularFile)
                    .filter(f -> f.toString().endsWith(".txt"))
                    .collect(Collectors.toList());

            for (Path ann : annFiles) {
                GeneralText data = new GeneralText();
                data.setText(Files.readString(ann));
                anonymizer.anonymize(data);
            }
        }
        long elapsedTime = System.nanoTime() - startTime;
        return String.valueOf("Elapsed seconds: " + elapsedTime/(1000000*1000));
    }

    public String runOnStudents() throws IOException, GazetteerException {
        List<PdeEntity> retrieved = new ArrayList<>();
        List<PdeEntity> hits = new ArrayList<>();
        List<PdeEntity> misses = new ArrayList<>();
        List<BratEntity> gold = new ArrayList<>();
        float totalRecall = 0f;
        int scanedCount = 0;

        int orderCloseness = 1; int indexCloseness = 30; int minIdentifierLength = 5; int minInputLength = 4;
        int minFirstNames = 0; boolean isUsingRules = true;

        UnstructuredTextAnonymizer unstructuredTextAnonymizer = new UnstructuredTextAnonymizer(null, null, null, null,
                orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);

        Path studentData = Paths.get("testdata", "students");
        List<Path> studentFolders = Files
                .walk(studentData, 1)
                .filter(f -> !studentData.equals(f))
                .filter(Files::isDirectory)
                .collect(Collectors.toList());

        for (Path studentFolder : studentFolders) {
            List<Path> annFiles = Files.walk(studentFolder, 1)
                    .filter(f -> !studentFolder.equals(f))
                    .filter(Files::isRegularFile)
                    .filter(f -> f.toString().endsWith(".ann"))
                    .collect(Collectors.toList());

            for (Path ann : annFiles) {
                AnnotationResult annResult = processAnnFilePdeMatch(ann, unstructuredTextAnonymizer);
                retrieved.addAll(annResult.getRetrieved());
                hits.addAll(annResult.getHits());
                misses.addAll(annResult.getMisses());
                gold.addAll(annResult.getGold());
                totalRecall += entityMatcher.calculateRecall(annResult.getRecall(), annResult.getGold().size());
                scanedCount++;
            }
        }

        StringBuilder sb = new StringBuilder();
        addEndRule(sb);
        addSection("Metrics", sb);
        sb.append("Scanned files (wo empty): " + scanedCount + "\n");
        sb.append("Total PDE entities: " + retrieved.size() + "\n");
        float prec = entityMatcher.calculateUnweightedPrecisison(hits.size(), retrieved.size());
        sb.append("Total precision: " + prec + "\n");
        float recl = entityMatcher.calculateUnweightedRecall(totalRecall, gold.size());
        sb.append("Total recall: " + recl + "\n");
        sb.append("Total fmeasure: " + entityMatcher.calculateFmeasure(prec, recl) + "\n");
        addEndRule(sb);

        addSection("Gold (Brat) entities", gold.size(), sb);
        gold.stream().collect(Collectors.groupingBy(BratEntity::getType, Collectors.counting()))
                .forEach((k,v) -> sb.append(k + ": " + v + "\n"));
        addEndRule(sb);

        addSection("Retrieved PDE", retrieved.size(), sb);
        retrieved.stream().collect(Collectors.groupingBy(PdeEntity::getType, Collectors.counting()))
                .forEach((k,v) -> sb.append(k + ": " + v + "\n"));
        addEndRule(sb);

        addSection("True positive PDE", hits.size(), sb);
        hits.stream().collect(Collectors.groupingBy(PdeEntity::getType, Collectors.counting()))
                .forEach((k,v) -> sb.append(k + ": " + v + "\n"));
        addEndRule(sb);

        addSection("False positive PDE", misses.size(), sb);
        misses.stream().collect(Collectors.groupingBy(PdeEntity::getType, Collectors.counting()))
                .forEach((k,v) -> sb.append(k + ": " + v + "\n"));
        addEndRule(sb);

        return sb.toString();
    }

    @Deprecated
    private AnnotationResult processAnnFile(Path ann, NerInMemoryRecognizer nerInMemoryRecognizer) throws IOException {
        final Path brat_annotated = ann;
        final Path plaintext =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".txt"));
        final Path html_result =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".html"));

        String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");
        ParsedLine parsedLine = nerInMemoryRecognizer.parseLine(plaintextContent);
        NerScanResult nerScanResult = new NerScanResult();
        nerScanResult.addLine(parsedLine);
        rulesMatcher.addRuleMatches(nerScanResult, plaintextContent);

        StandoffFile standoffFile = standoffReader.extractBratEntities(brat_annotated, "");

        Map<BratEntity, List<NerEntity>> entityMap =
                entityMatcher.combinedOverlapThreshold(
                        standoffFile.getEntities(), nerScanResult.getNerEntities());

        float precision = entityMatcher.getPrecision(entityMap, nerScanResult.getNerEntities());
        float recall = entityMatcher.getRecall(entityMap);
        float fmeasure = entityMatcher.getFmeasure(entityMap, nerScanResult.getNerEntities());

        Files.write(html_result, htmlGenerator.getHtmlResultPage(standoffFile.getEntities(),
                nerScanResult.getNerEntities(), plaintextContent, plaintext.getFileName().toString(),
                precision, recall, fmeasure).getBytes());

        long entitiesCount = standoffFile.getEntities().size();
        long count = (entitiesCount < 3) ? 0 : entitiesCount;
        float rec = (recall == 0 || count == 0) ? 0 : count * recall;
        return new AnnotationResult(rec, precision, fmeasure);
    }

    private AnnotationResult processAnnFilePdeMatch(Path ann, UnstructuredTextAnonymizer unstructuredTextAnonymizer) throws IOException {
        final Path plaintext =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".txt"));
        final Path html_result =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".html"));

        String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");

        // get Brat entities
        StandoffFile standoffFile = standoffReader.extractBratEntities(ann, "");
        Map<Boolean, List<BratEntity>> collectBrat = standoffFile.getEntities().stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getText())));
        List<BratEntity> bratEntities  = collectBrat.get(false);
        List<BratEntity> bratEntities_synthetic  = collectBrat.get(true);

        // get PDEs
        TreeSet<PdeEntity> anonymized_dirty = unstructuredTextAnonymizer.extractPdeEntities(plaintextContent);
        Map<Boolean, List<PdeEntity>> collectAnonymized = anonymized_dirty.stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getTextContent())));
        List<PdeEntity> anonymized = collectAnonymized.get(false);
        List<PdeEntity> anonymized_synthetic = collectAnonymized.get(true);

        // calculate overlaps
        Map<BratEntity, List<PdeEntity>> entityMap = entityMatcher.pdeOverlap(bratEntities, anonymized);
        Map<BratEntity, List<PdeEntity>> entityMap_synthetic = entityMatcher.pdeOverlap(bratEntities_synthetic, anonymized_synthetic);

        // calculate metrics
        float precision = entityMatcher.getPrecisionPde(entityMap, anonymized);
        float recall = entityMatcher.getRecallPde(entityMap, anonymized);
        float precision_synthetic = entityMatcher.getPrecisionPde(entityMap_synthetic, anonymized_synthetic);
        float recall_synthetic = entityMatcher.getRecallPde(entityMap_synthetic, anonymized_synthetic);

        // generate HTML
        List<NerEntity> flatPdes = anonymized.stream().map(PdeEntity::flatten).collect(Collectors.toList());
        Files.write(html_result,
                htmlGenerator.getHtmlResultPage(bratEntities, flatPdes, plaintextContent,
                        plaintext.getFileName().toString(), precision, recall, 0f).getBytes());

        // filter badly recognized synthetic data
        if (precision_synthetic < 0.99f || recall_synthetic < 0.99f) {
            return new AnnotationResult(0,0,0);
        }
        AnnotationResult annResult = new AnnotationResult(entityMatcher, precision, recall, bratEntities,
                new ArrayList<>(anonymized),
                new ArrayList<>(entityMatcher.getHits(entityMap, anonymized)),
                new ArrayList<>(entityMatcher.getMisses(entityMap, anonymized)));
        return annResult;
    }

    public String getEntityStatistics() throws IOException, GazetteerException {
        int orderCloseness = 1; int indexCloseness = 30; int minIdentifierLength = 5; int minInputLength = 4;
        int minFirstNames = 0; boolean isUsingRules = true;

        int cbrat = 0; int sbrat = 0; int canon = 0; int sanon = 0;

        UnstructuredTextAnonymizer unstructuredTextAnonymizer = new UnstructuredTextAnonymizer(null, null, null, null,
                orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);

        Path studentData = Paths.get("testdata", "students");
        List<Path> studentFolders = Files
                .walk(studentData, 1)
                .filter(f -> !studentData.equals(f))
                .filter(Files::isDirectory)
                .collect(Collectors.toList());

        for (Path studentFolder : studentFolders) {
            List<Path> annFiles = Files.walk(studentFolder, 1)
                    .filter(f -> !studentFolder.equals(f))
                    .filter(Files::isRegularFile)
                    .filter(f -> f.toString().endsWith(".ann"))
                    .collect(Collectors.toList());

            for (Path ann : annFiles) {
                int[] annResult = countAnnotations(ann, unstructuredTextAnonymizer);
                cbrat += annResult[0];
                sbrat += annResult[1];
                canon += annResult[2];
                sanon += annResult[3];
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Actual Brat entities: " + cbrat + "\n");
        sb.append("Synthetic Brat entities: " + sbrat + "\n");
        sb.append("Actual PDE entities: " + canon + "\n");
        sb.append("Synthetic PDE entities: " + sanon + "\n");
        sb.append("Total Brat: " + (cbrat + sbrat) + "\n");
        sb.append("Total PDE: " + (canon + sanon) + "\n");
        return sb.toString();
    }

    private int[] countAnnotations(Path ann, UnstructuredTextAnonymizer unstructuredTextAnonymizer) throws IOException {
        final Path plaintext =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".txt"));
        final Path html_result =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".html"));

        String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");

        // get Brat entities
        StandoffFile standoffFile = standoffReader.extractBratEntities(ann, "");
        Map<Boolean, List<BratEntity>> collectBrat = standoffFile.getEntities().stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getText())));
        List<BratEntity> bratEntities  = collectBrat.get(false);
        List<BratEntity> bratEntities_synthetic  = collectBrat.get(true);

        // get PDEs
        TreeSet<PdeEntity> anonymized_dirty = unstructuredTextAnonymizer.extractPdeEntities(plaintextContent);
        Map<Boolean, List<PdeEntity>> collectAnonymized = anonymized_dirty.stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getTextContent())));
        List<PdeEntity> anonymized = collectAnonymized.get(false);
        List<PdeEntity> anonymized_synthetic = collectAnonymized.get(true);

        return new int[] {bratEntities.size(), bratEntities_synthetic.size(), anonymized.size(), anonymized_synthetic.size()};
    }

    public String runOnStudentsBrat() throws IOException, GazetteerException {
        List<BratEntity> retrieved = new ArrayList<>();
        List<BratEntity> hits = new ArrayList<>();
        List<PdeEntity> gold = new ArrayList<>();
        float totalRecall = 0f;
        int scanedCount = 0;

        int orderCloseness = 1;
        int indexCloseness = 30;
        int minIdentifierLength = 5;
        int minInputLength = 4;
        int minFirstNames = 0;
        boolean isUsingRules = true;

        UnstructuredTextAnonymizer unstructuredTextAnonymizer = new UnstructuredTextAnonymizer(null, null, null, null,
                orderCloseness, indexCloseness, minIdentifierLength, minInputLength, minFirstNames, isUsingRules);

        Path studentData = Paths.get("testdata", "students");
        List<Path> studentFolders = Files
                .walk(studentData, 1)
                .filter(f -> !studentData.equals(f))
                .filter(Files::isDirectory)
                .collect(Collectors.toList());

        for (Path studentFolder : studentFolders) {
            List<Path> annFiles = Files.walk(studentFolder, 1)
                    .filter(f -> !studentFolder.equals(f))
                    .filter(Files::isRegularFile)
                    .filter(f -> f.toString().endsWith(".ann"))
                    .collect(Collectors.toList());

            for (Path ann : annFiles) {
                AnnotationResult annResult = processAnnFilePdeMatch_Annotators(ann, unstructuredTextAnonymizer);
                retrieved.addAll(annResult.retrieved_brat);
                hits.addAll(annResult.hits_brat);
                gold.addAll(annResult.gold_brat);
                totalRecall += entityMatcher.calculateRecall(annResult.getRecall(), annResult.gold_brat.size());
                if (annResult.gold_brat.size() > 0)  {
                    scanedCount++;
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        addEndRule(sb);
        addSection("Metrics", sb);
        sb.append("Scanned files (wo empty): " + scanedCount + "\n");
        sb.append("Total PDE entities: " + retrieved.size() + "\n");
        float prec = entityMatcher.calculateUnweightedPrecisison(hits.size(), retrieved.size());
        sb.append("Total precision: " + prec + "\n");
        float recl = entityMatcher.calculateUnweightedRecall(totalRecall, gold.size());
        sb.append("Total recall: " + recl + "\n");
        sb.append("Total fmeasure: " + entityMatcher.calculateFmeasure(prec, recl) + "\n");
        addEndRule(sb);
        return sb.toString();
    }

    private AnnotationResult processAnnFilePdeMatch_Annotators(Path ann, UnstructuredTextAnonymizer unstructuredTextAnonymizer) throws IOException {
        final Path plaintext =
                ann.getParent().resolve(ann.getFileName().toString().replace(".ann", ".txt"));

        String plaintextContent = Files.readString(plaintext).replaceAll("\\r\\n", "\n");

        // get PDEs
        TreeSet<PdeEntity> anonymized_dirty = unstructuredTextAnonymizer.extractPdeEntities(plaintextContent);
        Map<Boolean, List<PdeEntity>> collectAnonymized = anonymized_dirty.stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getTextContent())));
        List<PdeEntity> anonymized = collectAnonymized.get(true);


        // get Brat entities
        StandoffFile standoffFile = standoffReader.extractBratEntities(ann, "");
        Map<Boolean, List<BratEntity>> collectBrat = standoffFile.getEntities().stream()
                .collect(Collectors.partitioningBy(x -> syntheticCleaner.contains(x.getText())));
        List<BratEntity> bratEntities = collectBrat.get(true);

        if (anonymized.size() < bratEntities.size()) {
            List<PdeEntity> createed = new ArrayList<>();
            for (BratEntity b: bratEntities) {
                createed.add(new PdeEntity());
            }
            return new AnnotationResult(1f, 1.f,
                    createed, bratEntities, bratEntities);
        }

        // calculate overlaps
        Map<PdeEntity, List<BratEntity>> entityMap = entityMatcher.bratOverlap(bratEntities, anonymized);

        // calculate metrics
        float precision = entityMatcher.getPrecisionBrat(entityMap, standoffFile.getEntities());
        float recall = entityMatcher.getRecallBrat(entityMap, standoffFile.getEntities());

        AnnotationResult annResult = new AnnotationResult(precision, recall, new ArrayList<>(anonymized),
                bratEntities, new ArrayList<>(entityMatcher.getHitsBrat(entityMap, bratEntities)));
        return annResult;
    }

    private void addEndRule(StringBuilder sb) {
        sb.append("=====================================\n");
    }

    private void addSection(String title, int total, StringBuilder sb) {
        sb.append(title + " ...total: " + total + "\n");
    }

    private void addSection(String title, StringBuilder sb) {
        sb.append(title + "................\n");
    }

}
