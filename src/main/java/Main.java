import gazetteer.exceptions.GazetteerException;
import nametag.NameTagException;
import run.AggregateRunner;

import java.io.*;

public class Main {
    public static void main(String... args) throws IOException, InterruptedException, NameTagException, GazetteerException {
        AggregateRunner runner = new AggregateRunner();
//        System.out.println(runner.getAggregateRecall());
//        System.out.println(runner.getAggregateRecallInMemory());
//        System.out.println(runner.measureAnonThroughput());
//        System.out.println(runner.getEntityStatistics());
        System.out.println(runner.runOnStudents());
    }

}
